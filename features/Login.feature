@loginFeature
Feature: Login page
In order to allow user with right credentials to secure area
As a user wanting access
I must be see what is my login status

@javascript @loginAccess
Scenario: user with correct credentials
Given Bia is on Login page
When Bia try to login
Then she can see she is logged in

@javascript @loginError
Scenario: user with incorrect name
Given Bia is on Login page
When Bia try to login but forgot username
Then she will see an invalid username message

@javascript @loginError
Scenario: user with incorrect name
Given Bia is on Login page
When Bia try to login but forgot password
Then she will see an invalid password message