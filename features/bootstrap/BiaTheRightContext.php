<?php

use Behat\Behat\Context\SnippetAcceptingContext;
use Behat\Mink\Driver\Selenium2Driver;
use Behat\MinkExtension\Context\MinkContext as MinkContext;
use Features\Bootstrap\PageObjects\General as General;
use Behat\Mink\WebAssert;

require_once __DIR__ . "/locators.php";

/**
 * Defines application features from the specific context.
 */
class BiaTheRightContext extends MinkContext implements SnippetAcceptingContext {

    public function __construct() {
	$this->general = new General($this);
        $this->assert = new WebAssert($this->getSession());
    }
    protected $general;
    protected $assert;
    protected $login;

    /**
     * @BeforeStep
     */
    public function beforeStep(){
        $driver = $this->getSession()->getDriver();
        if ($driver instanceof Selenium2Driver) {
            $this->getSession()->resizeWindow(1920, 1080, 'current');
        }
    }
    
    /**
     * @Given Bia is on Login page
     */
    public function visitLoginPage(){
	    $this->login = $this->general->getLogin();
    	$this->login->visit();
    }
    
    /**
     * @When Bia try to login
     */
    public function login(){
    	$this->login->enterUserName();
        $this->login->enterUserPassword();
        $this->login->submit();
    }
    
    /**
     * @Then she can see she is logged in
     */
    public function logged(){
    	$this->assert->pageTextContains($isVisible, "You logged into a secure area!");
    }
    
    
}
