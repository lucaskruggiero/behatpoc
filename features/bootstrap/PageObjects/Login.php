<?php 

namespace Features\Bootstrap\PageObjects;

use Features\Bootstrap\PageObjects\General as General;

//require_once __DIR__ . "/../locators.php";
require_once __DIR__ . "/../configuration.php";
class Login extends General {

	protected $url = "http://the-internet.herokuapp.com/login";
	protected $userNameField = "username";
	protected $passWordField = "password";
	protected $form = "login";
	protected $userName = "tomsmith";
	protected $password = "SuperSecretPassword";
	protected $message = ".flash"


    public function __construct(MinkContext $context){
	  parent::__construct($context);
    }

	public function visitLoginPage(){
		$this->visit($this->url);
	}

	public function enterUserName($param = $this->userName){
		$this->waitElement($this->userNameField, 5);
		$this->fillField($this->userNameField, $param);
	}
	
	public function enterUserPassword($param = $this->password){
		$this->waitElement($this->PassWordField, 5);
		$this->fillField($this->PassWordField, $param);
	}
	
	public function submit(){
		$his->submit($this->form);
	}

    public function getMessageNode(){
		return $this->find('css', $this->message);
	}


}
