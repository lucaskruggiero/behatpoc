<?php

namespace Features\Bootstrap\PageObjects;

trait Helper{
    //minimize code
    protected function evaluateScript($param){
        return $this->context->getSession()->evaluateScript($param);
    }

    protected function findById( $param){
        return $this->context->getSession()->getPage()->findById( $param);
    }

    protected function find($object, $param){
        return $this->context->getSession()->getPage()->find($object, $param);
    }

    protected function findAll($object, $param){
        return $this->context->getSession()->getPage()->findAll($object, $param);
    }

    protected function executeScript($param){
        $this->context->getSession()->getPage()->executeScript($param);
    }

    protected function visit($param){
        $this->context->visitPath($param);
    }

    protected function fillField($field, $text){
	    $this->context->fillField($field, $text);
    }

    protected function pressButton($field){
	    $this->context->pressButton($field);
    }

    protected function submit($field){
	    $this->context->submit($field);
    }

    //Page helper
    public function waitElement($element, $time){
	    $i=0;

    	while(!$this->findById($element) && $i <= $time ){
	        sleep(1);
	        $i++;
	    }
        return;
    }
    
    public function viewElement($element, $time){
        $i=0;
        while(!$this->findById($element)->isVisible() && $i <= $time ){
	        sleep(1);
	        $i++;
	    }
        return;
    }

}

