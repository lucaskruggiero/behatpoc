<?php 

namespace Features\Bootstrap\PageObjects;

use Features\Bootstrap\PageObjects\Login;

require_once __DIR__ . "/../locators.php";

/**
 * Class to provide objects to context
 */
class General {
    use Helper;

    protected $context;
    private $login;

    public function __construct($context){
	    $this->context = $context;
    }

    public function getLogin(){
    	if ($this->login ==null){
	        $this->login = new Login($this->context);
    	}
	    return $this->login;
    }
}
