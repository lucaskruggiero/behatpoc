<?php

$locator = array(
		//For login
		"email" => "username",
		"password" => "password",
		"submit button" => "login-submit",
		
		//For adding restrictions
		"restriction" => "action-page-permissions-link",
		"menu" => "action-menu-link",
		"restriction dropdown" => "s2id_page_restrictions-dialog-selector",
		"add restriction" => "select2-drop",
		"restriction user" => "s2id_autogen6",
		"found user" => "select2-result-label",
		"add" => "page-restrictions-add-button",
		"user names" => "user-avatar-title",
		"no restrictions" => 1,
		"restrict edition" => 2,
		"Exibitions and restrictions editions" => 3,
		"apply" => "page-restrictions-dialog-save-button",
		"added users" => "data-username",	

		//for adding new pages
		"+" => "create-page-button",
		"blank" => 0,
		"blog post" => 1,
		"create button" => ".create-dialog-create-button",
		"title" => "content-title",
		"publish" => "rte-button-publish",
		"created page" => "title-text",
		"..." => "action-menu-link",
		"remove page" => "action-remove-content-link",
		"confirm delete" => "confirm"
);

define('LOCATORS', $locator);

