<?php

use Behat\Behat\Context\SnippetAcceptingContext;
use Behat\Mink\Driver\Selenium2Driver;
use Behat\MinkExtension\Context\MinkContext as MinkContext;
use Features\Bootstrap\PageObjects\General as General;
use Behat\Mink\WebAssert;

require_once __DIR__ . "/locators.php";

/**
 * Defines application features from the specific context.
 */
class LisaTheWrongContext extends MinkContext implements SnippetAcceptingContext {

    public function __construct() {
	$this->general = new General($this);
        $this->assert = new WebAssert($this->getSession());
    }
    protected $general;
    protected $assert;
    protected $login;

    /**
     * @BeforeStep
     */
    public function beforeStep(){
        $driver = $this->getSession()->getDriver();
        if ($driver instanceof Selenium2Driver) {
            $this->getSession()->resizeWindow(1920, 1080, 'current');
        }
    }
    
    /**
     * @Given Bia is on Login page
     */
    public function visitLoginPage(){
	    $this->login = $this->general->getLogin();
    	$this->login->visit();
    }
    
    /**
     * @When Bia try to login but forgot password
     */
    public function wrongPass(){
    	$this->login->enterUserName();
        $this->login->enterUserPassword("OhIforgotIT");
        $this->login->submit();
    }
    
    /**
     * @Then she will see an invalid password message
     */
    public function checkPasswordMessage(){
        $this->assert->pageTextContains($isVisible, "Your password is invalid!");
    }
    
    /**
     * @When Bia try to login but forgot username
     */
    public function wrondName(){
    	$this->login->enterUserName("LisaIGuess");
        $this->login->enterUserPassword();
        $this->login->submit();
    }
    
    /**
     * @Then she will see an invalid username message
     */
    public function checkNameMessage(){
        $this->assert->pageTextContains($isVisible, "Your username is invalid!");
    }
    
}
