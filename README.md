POC based on Behat using Docker
============

[![](https://images.microbadger.com/badges/version/bergil/docker-behat.svg)](https://microbadger.com/images/bergil/docker-behat) [![](https://images.microbadger.com/badges/image/bergil/docker-behat.svg)](https://microbadger.com/images/bergil/docker-behat)
============

It is necessary to install Docker CE to run this implementation:[Docker](https://docs.docker.com/engine/installation/)

After installed, pull this repository and cd to it.

run ```$ sudo docker built -t bergil/docker-behat/local .```

After the image is installed, you can run the test: 

- ```$ sudo sh test.sh --tags=page```

- ```$ sudo sh test.sh --tags=setRestriction```

[Image page on Docker Hub.](https://hub.docker.com/r/bergil/docker-behat/)
